Cookiecutter Ansible Role
=========================

**This repository is DEPRECATED**
New repository moved to https://gitlab.esss.lu.se/ics-infrastructure/cookiecutter-ics-ans-role


Cookiecutter_ template for ESS ICS Ansible_ role: ics-ans-role-<name>

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet::

    $ pip install cookiecutter

Generate an Ansible role project::

    $ cookiecutter git+https://bitbucket.org/europeanspallationsource/cookiecutter-ics-ans-role

Detailed instructions
---------------------

The role will be named `ics-ans-role-<name>`. `ics-ans-role` is automatically prepended.
Only enter `<name>` when asked for the role_name.

To create `ics-ans-role-foo`::

    $ cookiecutter git+https://bitbucket.org/europeanspallationsource/cookiecutter-ics-ans-role
    company [European Spallation Source ERIC]:
    full_name [Benjamin Bertrand]:
    role_name [ics-ans-role-<name>]: foo
    description [Ansible role to install foo]:
    has_dependencies [n]: y
    Select molecule_driver:
    1 - docker
    2 - vagrant
    Choose from 1, 2 [1]:

This creates the following project::

    ics-ans-role-foo/
    ├── LICENSE
    ├── README.md
    ├── defaults
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── molecule
    │   └── default
    │       ├── Dockerfile.j2
    │       ├── create.yml
    │       ├── destroy.yml
    │       ├── molecule.yml
    │       ├── playbook.yml
    │       ├── prepare.yml
    │       ├── requirements.yml
    │       └── tests
    │           └── test_default.py
    └── tasks
        └── main.yml

The project includes required files to test the role using Molecule_.

License
-------

BSD 2-clause license

.. _Ansible: http://docs.ansible.com/ansible/
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Molecule: http://molecule.readthedocs.io/en/master/
